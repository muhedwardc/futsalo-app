const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    text: String,
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        name: String,
        image: String
    },
    book: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Book'
        },
        date: String,
        fieldName: String
    },
    joined: []
})

module.exports = mongoose.model('Post', PostSchema);