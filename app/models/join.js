const mongoose = require('mongoose')

const JoinSchema = new mongoose.Schema({
    fieldName: String,
    data: {
        date: String,
        hour: Number
    },
    userID: {
        type: mongoose.Schema.Types.ObjectId
    }
})

module.exports = mongoose.model('Join', JoinSchema)