const mongoose = require('mongoose');

const FieldSchema = new mongoose.Schema({
    name: String,
    description: String,
    address: String,
    province: String,
    city: String,
    images: [],
    open: Number,
    closed: Number,
    price: Number,
    ownerID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    bookList: [
        {
            date: String,
            hours: [{
                hour: Number,
                bookerID: mongoose.Schema.Types.ObjectId,
                bookID: mongoose.Schema.Types.ObjectId
            }],
        }
    ]
})

module.exports = mongoose.model('Field', FieldSchema);