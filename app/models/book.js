const mongoose = require('mongoose');
        
const BookSchema = new mongoose.Schema({
    // data: {
    //     date: String,
    //     hour: Number
    // },
    // field: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'Field',
    //     required: true
    // },
    // owner: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'User',
    //     required: true
    // }
    ownerID: {
        type: mongoose.Schema.Types.ObjectId
    },
    field: {
        id: {
            type: mongoose.Schema.Types.ObjectId
        },
        name: String,
        city: String,
        province: String
    },
    data: {
        date: String,
        hour: Number
    }
})

module.exports = mongoose.model('Book', BookSchema);