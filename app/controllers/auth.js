const User = require('../models/user')
const passport = require('passport')

module.exports = {
    login: function(req, res) {
        res.render('login');
    },

    register: function(req, res) {
        res.render('register');
    },

    newLogin: function(req, res) {
        res.redirect('/profile');
    },

    newUser: function(req, res) {
        const data = req.body
        User.register(new User({
            name: data.name,
            username: data.username,
            email: data.email,
            phone: data.phone
        }), data.password, function(error, user){
            if (error) {
                console.log(error)
                return res.render('register')
            }

            passport.authenticate('local')(req, res, function() {
                res.redirect("/profile")
            })
        })
    },

    logout: function(req, res) {
        req.logout()
        res.redirect('/')
    }
}