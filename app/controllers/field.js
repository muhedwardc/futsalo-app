const Field = require('../models/field')
const Book = require('../models/book')

module.exports = {
    all: function(req, res) {
        const query = req.query
        if (Object.keys(query).length === 0 && query.constructor === Object) {
            Field.find({}, function(error, fields) {
                if (error) console.log(error)
                else {
                    res.render('fields', {fields, title: 'Futsalo - Fields'})
                }
            })
        } else {
            if (typeof query.province !== 'undefined') {
                Field.find({"province": query.province}, function(error, fields) {
                    if (error) console.log(error)
                    else {
                        if (typeof query.city !== 'undefined') {
                            Field.find({"city": query.city}, function(error, fields) {
                                res.render('fields', {fields, title: 'Futsalo - Fields'})
                            })
                        } else {
                            res.render('fields', {fields, title: 'Futsalo - Fields'})
                        }
                    }
                })
            } else {
                Field.find({}, function(error, fields) {
                    if (error) console.log(error)
                    else {
                        res.render('fields', {fields, title: 'Futsalo - Fields'})
                    }
                })
            }
        }
    },

    detail: function(req, res) {
        const id = req.params.id;
        let recommendations = []
        let currentField;
        Field.findById(id, function(error, field) {
            if (error) console.log(error)
            else {
                currentField = field;
                Field.find({$and: [{"city": field.city}, {"_id": {$ne: field._id}}]}, function(error, fields) {
                    if (error) console.log(error)
                    else {
                        if (fields.length < 5) {
                            let ids = [currentField._id]
                            fields.forEach(function(field){
                                ids.push(field._id)
                                recommendations.push(field)
                            });
                            Field.find({$and: [{"province": field.province}, {"_id": {$nin: ids}}]}, function(error, fields) {
                                if (error) console.log(error)
                                else {
                                    recommendations = [...recommendations, ...fields]
                                    res.render('detail', {title: currentField.name, field: currentField, recommendations: recommendations.slice(0, 5)})        
                                }
                            })
                        } else {
                            res.render('detail', {title: currentField.name, field: currentField, recommendations: fields.slice(0, 5)})
                        }
                    }
                })
            }
        })
    },

    create: function(req, res) {
        res.render('createField')
    },

    newField: function(req, res) {
        const data = req.body
        const newFieldData = {
            name: data.name,
            address: data.address,
            province: data.province,
            city: data.city,
            description: data.description,
            open: Number(data.open.split('.')[0]),
            closed: Number(data.closed.split('.')[0]),
            price: Number(data.price),
            ownerID: req.user._id
        }

        if (req.files) {
            newFieldData.images = [];
            for (let item of req.files) {
                newFieldData.images.push(item.url);
            }
        }

        const newField = new Field(newFieldData);

        Field.create(newField, function(error, createdField) {
            if (error) console.log(error)
            else {
                res.redirect('/fields/' + createdField._id);
            }
        })
    },

    getCheckout: function(req, res) {
        res.render('checkout')
    },

    checkout: function(req, res) {
        const fieldID = req.params.id
        const user = req.user._id
        let selected = req.body.data.split('.00')
        selected = selected.splice(0, selected.length - 1)
        let data = []

        Field.findById(fieldID, function(error, field) {
            if (error) res.redirect('/')
            else {                
                for (let i = 0; i < selected.length; i += 2) {
                    let splitted = selected[i].split(', ');
                    let date = splitted[1].split(' ').join('-')
                    let hour = Number(splitted[2])
                    
                    let newBook = new Book({
                        ownerID: user,
                        field: {
                            id: field._id,
                            name: field.name,
                            city: field.city,
                            province: field.province
                        },
                        data: {
                            date: date,
                            hour: hour
                        }
                    })
                    Book.create(newBook, function(error, book) {
                        if (error) console.log(error)
                        else {
                            data.push({date: date, hours: []})
                            data.forEach(function(item) {
                                if (item.date == date) {
                                    item.hours.push({
                                        hour: hour,
                                        bookerID: user,
                                        bookID: book._id
                                    })
                                }
                            }) 

                            Field.findOneAndUpdate({"_id": fieldID}, {$push: {bookList: data}}, function(error, field) {
                                if (error) console.log(error)
                                else {
                                    res.redirect('/checkout')
                                }
                            })
                        }
                    })
                }

            }
        })
    }
}