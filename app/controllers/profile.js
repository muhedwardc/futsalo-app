const Field = require('../models/field')
const Book = require('../models/book')
const Join = require('../models/join')
const User = require('../models/user')
const Post = require('../models/post')
const mongoose = require('mongoose')
const multer = require('multer')

module.exports = {
    index: function(req, res) {
        let ownedFields, booked, joined, posted
        Field.find({"ownerID": req.user._id})
            .exec()
            .then(fields => {
                ownedFields = [...fields]
                Book.find({"ownerID": req.user._id})
                    .exec()
                    .then(fields => {
                        booked = [...fields]
                        Join.find({"userID": req.user._id})
                            .exec()
                            .then(joins => {
                                joined = [...joins]
                                Post.find()
                                    .exec()
                                    .then(posts => {
                                        posted = [...posts]
                                        res.render('profile', {ownedFields, booked, joined, posted})
                                    })
                                    .catch(error => {
                                        console.log(error)
                                        res.redirect('/profile')
                                    })
                            })
                            .catch(error => {
                                console.log(error)
                                res.redirect('/profile')
                            }) 
                    })
                    .catch(error => {
                        console.log(error)
                        res.redirect('/profile')
                    })
            })
            .catch(error => {
                console.log(error)
                res.redirect('/profile')
            })
    },

    editPage: function(req, res) {
        res.render('profileEdit');
    },

    edit: function(req, res) {
        let update = {
            name: req.body.name,
            username: req.body.username,
            phone: req.body.phone
        }

        req.file ? update.image = req.file.url : null

        User.findByIdAndUpdate(req.user._id, update, function(error, updatedUser) {
            if (error) res.redirect('/profile/' + req.user._id + '/edit')
            else {
                User.findById(req.user._id)
                    .exec()
                    .then(user => {
                        Post.updateMany(
                            {
                                "author.id": user._id
                            }, 
                            {
                                $set: {
                                    "author.name": user.name,
                                    "author.image": user.image
                                }
                            })
                            .exec()
                            .then(updated => {
                                res.redirect('/profile')
                            })
                    })
            }
        })
    },

    newPost: function(req, res) {
        Book.findById(req.body.bookID)
            .exec()
            .then(book => {
                User.findById(req.user._id)
                    .exec()
                    .then(user => {
                        const newPost = new Post({
                            text: req.body.description,
                            author: {
                                id: user._id,
                                name: user.name,
                                image: user.image
                            },
                            book: {
                                id: book._id,
                                date: book.data.date.split('-').join(' ') 
                                    + ' ('
                                    + (Number(book.data.hour) < 10 ? '0' + book.data.hour : book.data.hour)
                                    + '.00 - '
                                    + (Number(book.data.hour + 1) < 10 ? '0' + (book.data.hour + 1) : (book.data.hour + 1))
                                    + '.00)',
                                fieldName: book.field.name
                            }
                        })
                        newPost.save()
                            .then(newPost => {
                                res.redirect('/profile')
                            })
                            .catch(error => {
                                console.log(error)
                                res.redirect('/profile')
                            })
                    })

            })

    },

    join: function(req, res) {
        Post.findById(req.params.id)
            .exec()
            .then(post => {
                post.joined.push(req.user._id)
                post.save()
                    .then(post => {
                        res.redirect('/profile')
                    })
                    .catch(error => {
                        console.log(error)
                        res.redirect('/404')
                    })
            })
            .catch(error => {
                console.log(error)
                res.redirect('/404')
            })
    },

    deletePost: function(req, res) {
        Post.findById(req.params.id)
            .exec()
            .then(post => {
                if (post.author.id.toString() == req.user._id.toString()) {
                    post.remove()
                        .then(result => {
                            res.redirect('/profile')
                        })
                        .catch(error => {
                            console.log(error)
                            res.redirect('/profile')
                        })
                } else {
                    res.redirect('/404')
                }
            })
    },

    cancel: function(req, res) {
        Post.findById(req.params.id)
            .exec()
            .then(post => {
                let i = post.joined.indexOf(req.user._id)
                post.joined.splice(i, 1)
                post.save()
                    .then(post => {
                        res.redirect('/profile')
                    })
                    .catch(error => {
                        console.log(error)
                        res.redirect('/profile')
                    }) 
            })
            .catch(error => {
                console.log(error)
                res.redirect('/profile')
            }) 
    }
}