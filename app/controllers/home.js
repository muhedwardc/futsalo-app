module.exports = {
    index: function(req, res) {
        return res.render('home');
    },

    notFound: function(req, res) {
        return res.render('404', {
            title: 'Halaman tidak ditemukan'
        })
    }
}