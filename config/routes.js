
const home = require('../app/controllers/home')
const auth = require('../app/controllers/auth')
const field = require('../app/controllers/field')
const profile = require('../app/controllers/profile')
const about = require('../app/controllers/about')
const isAuth = require('./isAuthenticated')
const mongoose = require('mongoose')
const multer = require('multer')
const cloudinary = require("cloudinary")
const cloudinaryStorage = require("multer-storage-cloudinary")

module.exports = function(app, passport) {
    cloudinary.config({
        cloud_name: 'muhechaplay',
        api_key: '963342225476343',
        api_secret: '3qalwAG-XiEb3_8AWJNWCJ8K0FI'
    });

    const multerConf = {
        storage: cloudinaryStorage({
            cloudinary: cloudinary,
            folder: "profiles",
            allowedFormats: ["jpg", "png"]
        })
    }

    const multerConfField = {
        storage: cloudinaryStorage({
            cloudinary: cloudinary,
            folder: "fields",
            allowedFormats: ["jpg", "png"]
        })
    }

    app.get('/', home.index);

    // auth
    app.get('/login', isAuth.false, auth.login);
    app.get('/register', isAuth.false, auth.register);
    app.get('/logout', isAuth.true, auth.logout);
    app.post('/login', passport.authenticate('local', {
        failureRedirect: '/login'
    }), auth.newLogin);
    app.post('/register', auth.newUser);

    // fields
    app.get('/fields', field.all);
    app.get('/fields/create', isAuth.true, field.create);
    app.get('/fields/:id', field.detail);
    app.post('/fields', multer(multerConfField).array('fieldImages', 12), field.newField);
    app.post('/fields/:id/checkout', isAuth.true, field.checkout)

    // checkout
    app.get('/checkout', isAuth.true, field.getCheckout)

    // profile
    app.get('/profile', isAuth.true, profile.index);
    app.get('/profile/:id/edit', isAuth.true, profile.editPage);
    app.put('/profile', isAuth.true, multer(multerConf).single('profileImage'), profile.edit);
    app.post('/profile', isAuth.true, profile.newPost);
    app.get('/p/:id/join', isAuth.true, profile.join);
    app.get('/p/:id/delete', isAuth.true, profile.deletePost);
    app.get('/p/:id/cancel', isAuth.true, profile.cancel);

    //about
    app.get('/about', about.index);

    app.get('*', home.notFound);
}