module.exports = {
    true: function(req, res, next) {
        if (req.isAuthenticated()) next()
        else res.redirect('/login')
    },

    false: function(req, res, next) {
        if (req.isAuthenticated()) res.redirect('/')
        else next()
    }
}