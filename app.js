const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const db = require('./config/database')
const session = require('express-session')
const passport = require('passport')
const LocalStrategy = require('passport-local')
const methodOverride = require('method-override')
const multer = require('multer')
const routes = './config/routes'

const port = process.env.PORT || 8080

// models
const User = require('./app/models/user')

const app = express()

app.use('/uploads', express.static('uploads'))
app.use(express.static(path.join(__dirname, 'public')))
app.set('views', path.join(__dirname, 'app/views'))
app.set('view engine', 'ejs')

app.use(bodyParser.urlencoded({extended: true}))
app.use(methodOverride('_method'))

mongoose.connect(db.url, {useNewUrlParser: true})

app.use(session({
    secret: 'Futsalo is developed by Muhammad Edward Chakim',
    resave: false,
    saveUninitialized: false
}))

app.use(passport.initialize())
app.use(passport.session())

passport.use(new LocalStrategy(User.authenticate()))
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())

app.use(function(req, res, next) {
    res.locals.user = req.user
    next()
})

require(routes)(app, passport);

app.listen(port, () => {
    console.log('Futsalo server is running on port ' + port)
})
